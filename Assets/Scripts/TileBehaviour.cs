﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TileBehaviour : MonoBehaviour
{
    public float speed = 4;
    public int value;
    public Sprite normalSprite;
    public Sprite selectedSprite;
    [Header("References")]
    public SpriteRenderer mainRenderer;
    public SpriteRenderer valueRenderer;
    public AudioClip onDestinationReachedSound;
    public UnityEvent onDestinationReached;

    private Vector2 destination;
    private bool selected;
    private Vector3 startScale;

    public bool Selected
    {
        set
        {
            selected = value;
            mainRenderer.sprite = selected ? selectedSprite : normalSprite;
        }
        get
        {
            return selected;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        startScale = transform.localScale;
    }

    // Tile will move towards destination at speed defined by "speed" and invoke a UnityEvent and play a sound when reached
    void Update()
    {
        Vector2 position = new Vector2(transform.position.x, transform.position.y);
        if(position != destination)
        {
            transform.position = Vector2.MoveTowards(transform.position, destination, speed * Time.deltaTime);
            position = new Vector2(transform.position.x, transform.position.y);
            if (position == destination)
            {
                onDestinationReached.Invoke();
                GameManager.instance.audioManager.PlaySound(onDestinationReachedSound);
            }
        }
    }

    /// <summary>
    /// This function sets the tile's actual position to passed position
    /// </summary>
    /// <param name="_position">the position to be set to the tile</param>
    public void SetPosition(Vector2 _position)
    {
        transform.position = _position;
    }

    /// <summary>
    /// This functions sets the destination the tile will reach
    /// </summary>
    /// <param name="_destination">the destination to reach</param>
    public void SetDestination(Vector2 _destination)
    {
        destination = _destination;
    }

    /// <summary>
    /// This function sets the value of the tile and changes the sprite based on it
    /// </summary>
    /// <param name="_value">the value to set</param>
    public void SetValue(int _value)
    {
        value = _value;
        valueRenderer.sprite = GameManager.instance.tileValues[value];
    }

    /// <summary>
    /// This function sets the scale of the tile
    /// </summary>
    /// <param name="modifier">the value to multiply to scale the tile</param>
    public void SetScale(Vector2 modifier)
    {
        transform.localScale = new Vector3(transform.localScale.x * modifier.x, transform.localScale.y * modifier.y, transform.localScale.z);
    }
}

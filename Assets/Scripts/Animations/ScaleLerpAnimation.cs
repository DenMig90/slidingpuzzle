﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleLerpAnimation : LerpAnimation<Vector3>
{
    protected override void SetValues(float lerp)
    {
        transform.localScale = Vector3.Lerp(from, to, lerp);
    }
}

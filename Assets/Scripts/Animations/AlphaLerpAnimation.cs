﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlphaLerpAnimation : LerpAnimation<float>
{
    private Image image;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    protected override void SetValues(float lerp)
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, Mathf.Lerp(from, to, lerp));
    }
}

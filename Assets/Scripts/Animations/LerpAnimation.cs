﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpAnimation<T> : MonoBehaviour
{
    public T from;
    public T to;

    public float duration;
    public float delay;
    public AnimationCurve curve;
    public bool onStart = true;
    public bool unscaled = true;

    private void OnEnable()
    {
        if(onStart)
            StartAnimation();
    }

    /// <summary>
    /// This function start the animation
    /// </summary>
    public void StartAnimation()
    {
        StopAllCoroutines();
        StartCoroutine(AnimationCoroutine());
    }

    protected virtual void SetValues(float lerp)
    {
        // to be implemented in derived classes
    }

    /// <summary>
    /// This coroutine performs the animation
    /// </summary>
    /// <returns></returns>
    protected IEnumerator AnimationCoroutine()
    {
        float time = 0;
        float lerp = 0;
        SetValues(curve.Evaluate(lerp));
        while (time < delay)
        {
            yield return new WaitForEndOfFrame();
            time += unscaled ? Time.unscaledDeltaTime : Time.deltaTime;
        }
        time = 0;
        while (time < duration)
        {
            yield return new WaitForEndOfFrame();
            time += unscaled ? Time.unscaledDeltaTime : Time.deltaTime;
            SetValues(curve.Evaluate(time / duration));
        }
    }
}

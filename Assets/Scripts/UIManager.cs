﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UIManager : MonoBehaviour
{
    public Text timeText;
    public Text movesText;
    public Toggle musicMuteToggle;
    public Toggle soundMuteToggle;
    public GameObject pausePage;
    public GameObject settingsPage;
    public GameObject winPage;
    public UnityEvent onMovesUpdated;

    public bool AnyMenuOpened
    {
        get
        {
            return pausePage.activeSelf || winPage.activeSelf || settingsPage.activeSelf;
        }
    }

    private void Awake()
    {
        GameManager.instance.uiManager = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        pausePage.SetActive(false);
        winPage.SetActive(false);
        settingsPage.SetActive(false);
        musicMuteToggle.isOn = GameManager.instance.CurrentSave.musicMuted;
        soundMuteToggle.isOn = GameManager.instance.CurrentSave.soundMuted;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// This function opens the pause menu and sets the timescale to 0
    /// </summary>
    public void OpenPauseMenu()
    {
        Time.timeScale = 0;
        pausePage.SetActive(true);
    }

    /// <summary>
    /// This function closes the pause menu and sets the timescale to 1
    /// </summary>
    public void ClosePauseMenu()
    {
        Time.timeScale = 1;
        pausePage.SetActive(false);
    }

    /// <summary>
    /// This function opens the settings menu and sets the timescale to 0
    /// </summary>
    public void OpenSettingsMenu()
    {
        Time.timeScale = 0;
        settingsPage.SetActive(true);
    }

    /// <summary>
    /// This function closes the settings menu, sets the timescale to 1 and updates the save data
    /// </summary>
    public void CloseSettingsMenu()
    {
        Time.timeScale = 1;
        settingsPage.SetActive(false);
        GameManager.instance.WriteSaveState();
    }

    /// <summary>
    /// This function opens the win page
    /// </summary>
    public void OpenWinPage()
    {
        winPage.SetActive(true);
    }

    /// <summary>
    /// This function updates the UI moves text and invokes the relative unityEvent if passed value is different than 0
    /// </summary>
    /// <param name="moves">the moves number to be set</param>
    public void UpdateMoves(int moves)
    {
        movesText.text = moves.ToString();
        if (moves != 0)
            onMovesUpdated.Invoke();
    }

    /// <summary>
    /// This function updates the UI time text
    /// </summary>
    /// <param name="moves">the time in seconds to be set</param>
    public void UpdateTime(float time)
    {
        int minutes = (int)time / 60;
        timeText.text = minutes.ToString() + ":" + ((int)time - minutes * 60f).ToString("00");
    }

    /// <summary>
    /// This function calls the reload of the level and sets the timescale to 1
    /// </summary>
    public void ReloadCall()
    {
        Time.timeScale = 1;
        GameManager.instance.Reload();
    }

    /// <summary>
    /// This function sets the audioManager music muted state based on toggle value
    /// </summary>
    /// <param name="value">toggle checked value</param>
    public void MusicMuteToggleChanged(bool value)
    {
        GameManager.instance.audioManager.MuteMusic(value);
    }

    /// <summary>
    /// This function sets the audioManager sound muted state based on toggle value
    /// </summary>
    /// <param name="value">toggle checked value</param>
    public void SoundMuteToggleChanged(bool value)
    {
        GameManager.instance.audioManager.MuteSound(value);
    }
}

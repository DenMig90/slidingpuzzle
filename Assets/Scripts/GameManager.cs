﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GridType
{
    ThreeXThree = 3,
    FourXFour = 4
}

public class Cell
{
    private TileBehaviour tile;
    private Bounds bounds;

    public TileBehaviour Tile
    {
        set
        {
            tile = value;
            if (tile != null)
            {
                tile.SetDestination(Position);
            }
        }
        get { return tile; }
    }

    public Vector2 Position
    {
        get { return bounds.center; }
        set
        {
            bounds.center = value;
        }
    }

    public Vector2 Extents
    {
        set { bounds.extents = value; }
        get { return bounds.extents; }
    }

    public Bounds Bounds
    {
        get { return bounds; }
    }

    public bool IsEmpty
    {
        get
        {
            return Tile == null;
        }
    }
}

[System.Serializable]
public class Indexes
{
    public int column;
    public int row;

    public Indexes(int _row, int _column)
    {
        column = _column;
        row = _row;
    }
}

[System.Serializable]
public class SaveState
{
    public bool musicMuted = false;
    public bool soundMuted = false;
}

public class GameManager : MonoBehaviour
{
    [HideInInspector]
    public static GameManager instance;
    public GridType gridType;
    public TileBehaviour tilePrefab;
    public Vector2 cellSize = new Vector2(1, 1);
    public Sprite[] tileValues;
    public AudioClip gameMusic;
    public AudioClip winSound;
    public UIManager uiManager;
    public AudioManager audioManager;

    private int gridWidth = 4;
    private Cell[,] grid;
    private Indexes selectedIndexes;
    private int moves;
    private float winTime;
    private string saveName = "SaveState";

    public SaveState CurrentSave
    {
        private set;
        get;
    }

    public int Moves
    {
        private set
        {
            moves = value;
            uiManager.UpdateMoves(moves);
        }
        get { return moves; }
    }

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        gridWidth = (int)gridType;
        if(tileValues.Length < ((gridWidth*gridWidth)-1))
        {
            Debug.LogError("Elements in tileValues must be at least " + ((gridWidth * gridWidth) - 1));
            return;
        }
        GenerateGrid();
        RandomizeGrid();
        winTime = 0;
        LoadSaveState();
        audioManager.PlayMusic(gameMusic);
    }

    // Update is called once per frame
    void Update()
    {
        // Game Mechanics can be performed only if no menus are opened
        if (!uiManager.AnyMenuOpened)
        {
            // When mouse is clicked on a tile, it will store the tile's cell and select the tile
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                selectedIndexes = GetCellIndexes(mousePosition);
                if (selectedIndexes != null)
                {
                    if (grid[selectedIndexes.row, selectedIndexes.column].Tile)
                    {
                        grid[selectedIndexes.row, selectedIndexes.column].Tile.Selected = true;
                    }
                    else
                    {
                        selectedIndexes = null;
                    }
                }
            }

            // When mouse is moved while clicked, it will check if a cell different then the one in which the tile is currently positioned
            // If so, it will check if in the movement direction there is an empty cell
            // In that case it will move selected tile and its following ones toward the empty space making a move and clearing the selected cell
            if (Input.GetMouseButton(0))
            {
                if (selectedIndexes != null)
                {
                    Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Indexes actualIndexes = GetCellIndexes(mousePosition);
                    if (actualIndexes != null && (selectedIndexes.row != actualIndexes.row || selectedIndexes.column != actualIndexes.column))
                    {
                        if (selectedIndexes.row == actualIndexes.row || selectedIndexes.column == actualIndexes.column)
                        {
                            bool vertical = selectedIndexes.column == actualIndexes.column;
                            bool forward = vertical ? selectedIndexes.row < actualIndexes.row : selectedIndexes.column < actualIndexes.column;
                            Indexes emptyIndexes = GetEmptyIndexes(selectedIndexes, vertical, forward);
                            if (emptyIndexes != null)
                            {
                                MoveTiles(selectedIndexes, emptyIndexes);
                                Moves++;
                                selectedIndexes = actualIndexes;
                                ClearSelectedIndexes();
                            }
                        }
                    }
                }
            }

            // When the mouse is released before doing a move, the selected cell will be cleared
            if (Input.GetMouseButtonUp(0))
            {
                ClearSelectedIndexes();
            }
        }

        // When the user presses escape the game will quit
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    // It updates the time passed since game start and checks if win condition is met if winTime is not set
    private void LateUpdate()
    {
        if (winTime == 0)
        {
            uiManager.UpdateTime(Time.timeSinceLevelLoad);
            CheckWinCondition();
        }
    }

    /// <summary>
    /// This function generates the grid and its cells and instantiates the tiles
    /// </summary>
    private void GenerateGrid()
    {
        int counter = 0;
        grid = new Cell[gridWidth, gridWidth];
        for (int i = gridWidth - 1; i >= 0; i--)
        {
            float yPosition = (i - (gridWidth / 2f) + 0.5f) * cellSize.y;
            for (int j = 0; j < gridWidth; j++)
            {
                float xPosition = (j - (gridWidth / 2f) + 0.5f) * cellSize.x;
                grid[i, j] = new Cell();
                grid[i, j].Position = new Vector2(xPosition, yPosition);
                grid[i, j].Extents = cellSize / 2f;
                if (!(i == 0 && j == gridWidth - 1))
                {
                    TileBehaviour tile = Instantiate(tilePrefab, transform);
                    tile.name = "Tile" + (counter + 1);
                    tile.SetValue(counter);
                    tile.SetPosition(grid[i, j].Position);
                    tile.SetScale(cellSize);
                    grid[i, j].Tile = tile;
                }
                counter++;
            }
        }
    }

    /// <summary>
    /// This function randomizes the tiles' positions until the configuration is not solvable
    /// </summary>
    private void RandomizeGrid()
    {
        do
        {
            for (int i = 0; i < gridWidth; i++)
            {
                for (int j = 0; j < gridWidth; j++)
                {
                    int randomRow;
                    int randomColumn;
                    do
                    {
                        randomRow = Random.Range(0, gridWidth);
                        randomColumn = Random.Range(0, gridWidth);
                    } while (i == randomRow && j == randomColumn);
                    TileBehaviour aux = grid[i, j].Tile;
                    grid[i, j].Tile = grid[randomRow, randomColumn].Tile;
                    if (!grid[i, j].IsEmpty)
                    {
                        grid[i, j].Tile.SetPosition(grid[i, j].Position);
                    }
                    grid[randomRow, randomColumn].Tile = aux;
                    if (!grid[randomRow, randomColumn].IsEmpty)
                    {
                        grid[randomRow, randomColumn].Tile.SetPosition(grid[randomRow, randomColumn].Position);
                    }
                }
            }
        } while (!CheckSolvability());
    }

    /// <summary>
    /// This function makes a copy of current tiles positions presenting them in single array and returning how many inversions there are.
    /// An inversion occurs when a tile appears before another that has a smaller value
    /// </summary>
    /// <returns>the inversions number</returns>
    private int GetInversions()
    {
        int inversionsSum = 0;
        TileBehaviour[] tiles = new TileBehaviour[gridWidth * gridWidth];
        int counter = 0;

        for (int i = gridWidth-1; i >= 0; i--)
        {
            for (int j = 0; j < gridWidth; j++)
            {
                tiles[counter] = grid[i, j].Tile;
                counter++;
            }
        }

        for (int i = 0; i < tiles.Length; i++)
        {
            int thisTileInvertion = 0;
            for (int j = i; j < tiles.Length; j++)
            {
                if (tiles[j] != null && tiles[i]!= null)
                {
                    if (tiles[i].value > tiles[j].value)
                    {
                        thisTileInvertion++;
                    }
                }
            }
            inversionsSum += thisTileInvertion;
        }
        return inversionsSum;
    }

    /// <summary>
    /// This function returns if actual tiles' configuration is solvable based on the grid width, the number of inversions is even or odd, and the position of the empty cell
    /// </summary>
    /// <returns></returns>
    private bool CheckSolvability()
    {
        if(gridWidth % 2 == 0)
        {
            int emptyRow = -1;
            for (int i = 0; i < gridWidth && emptyRow == -1; i++)
            {
                for (int j = 0; j < gridWidth; j++)
                {
                    if(grid[i, j].IsEmpty)
                    {
                        emptyRow = i;
                        break;
                    }
                }
            }

            bool inversionsEven = GetInversions() % 2 == 0;
            return (inversionsEven && emptyRow % 2 == 0) || (!inversionsEven && emptyRow % 2 != 0);
        }
        else
        {
            return GetInversions() % 2 == 0;
        }
    }

    /// <summary>
    /// This function check if current tiles' configuration consists of tiles in a crescent order
    /// </summary>
    private void CheckWinCondition()
    {
        int counter = 0;
        bool win = true;
        for (int i = gridWidth - 1; i >= 0; i--)
        {
            float yPosition = (i - (gridWidth / 2f) + 0.5f) * cellSize.y;
            for (int j = 0; j < gridWidth; j++)
            {
                if (!(i == 0 && j == gridWidth - 1))
                {
                    if (grid[i,j].IsEmpty || grid[i, j].Tile.value != counter)
                    {
                        win = false;
                    }
                }
                counter++;
            }
        }
        if(win)
        {
            WinManagement();
        }
    }

    /// <summary>
    /// This function returns the indexes of the cell that contains passed position
    /// </summary>
    /// <param name="position">the position to check</param>
    /// <returns>the indexes of the cell or null if there isn't any</returns>
    private Indexes GetCellIndexes(Vector2 position)
    {
        for (int i = 0; i < gridWidth; i++)
        {
            for (int j = 0; j < gridWidth; j++)
            {
                if (grid[i, j].Bounds.Contains(position))
                {
                    return new Indexes(i, j);
                }
            }
        }
        return null;
    }

    /// <summary>
    /// This function returns the indexes of the empty cell if it is in the row or column of the start indexes continuing in the passed direction
    /// </summary>
    /// <param name="start">the indexes where to start checking</param>
    /// <param name="vertical">if the direction is vertical or horizontal</param>
    /// <param name="forward">if the direction is forward or backward</param>
    /// <returns>the indexes of the empty cell or null if it could not be found</returns>
    private Indexes GetEmptyIndexes(Indexes start, bool vertical, bool forward)
    {
        if (vertical)
        {
            if (forward)
            {
                for (int i = start.row + 1; i < gridWidth; i++)
                {
                    if (grid[i, start.column].IsEmpty)
                    {
                        return new Indexes(i, start.column);
                    }
                }
            }
            else
            {
                for (int i = start.row - 1; i >= 0; i--)
                {
                    if (grid[i, start.column].IsEmpty)
                    {
                        return new Indexes(i, start.column);
                    }
                }
            }
        }
        else
        {
            if (forward)
            {
                for (int i = start.column + 1; i < gridWidth; i++)
                {
                    if(grid[start.row, i].IsEmpty)
                    {
                        return new Indexes(start.row, i);
                    }
                }
            }
            else
            {
                for (int i = start.column - 1; i >= 0; i--)
                {
                    if (grid[start.row, i].IsEmpty)
                    {
                        return new Indexes(start.row, i);
                    }
                }
            }
        }
        return null;
    }

    /// <summary>
    /// This function performs the movement on the tiles starting from start moving all the tiles there are before the empty space passed as end
    /// </summary>
    /// <param name="start">the starting tile to move</param>
    /// <param name="end">the position of the empty space</param>
    private void MoveTiles(Indexes start, Indexes end)
    {
        int increment = 0;
        if (start.column != end.column && start.row != end.row)
        {
            Debug.LogError("MoveTiles: start and end parameters must have a common Index");
            return;
        }
        bool vertical = start.column == end.column;
        if(vertical)
        {
            increment = (start.row <= end.row) ? 1 : -1;
            int column = start.column;
            TileBehaviour temp = grid[end.row, column].Tile;
            for(int i = end.row; i != start.row;i-=increment)
            {
                int next = i - increment;
                grid[i, column].Tile = grid[next, column].Tile;
            }
            grid[start.row, column].Tile = temp;
        }
        else
        {
            increment = (start.column <= end.column) ? 1 : -1;
            int row = start.row;
            TileBehaviour temp = grid[row, end.column].Tile;
            for (int i = end.column; i != start.column; i -= increment)
            {
                int next = i - increment;
                grid[row, i].Tile = grid[row, next].Tile;
            }
            grid[row, start.column].Tile = temp;
        }
    }

    /// <summary>
    /// This function sets the selectedIndexes to null and deselects the previous selected tile
    /// </summary>
    private void ClearSelectedIndexes()
    {
        if (selectedIndexes != null)
        {
            if (!grid[selectedIndexes.row, selectedIndexes.column].IsEmpty)
            {
                grid[selectedIndexes.row, selectedIndexes.column].Tile.Selected = false;
            }
        }
        selectedIndexes = null;
    }

    /// <summary>
    /// This function stores the time when the game is won and opens the relative UI page, stops the music and play the relative sound
    /// </summary>
    private void WinManagement()
    {
        winTime = Time.timeSinceLevelLoad;
        audioManager.StopMusic();
        uiManager.OpenWinPage();
        audioManager.PlaySound(winSound);
    }

    /// <summary>
    /// This function loads the current saved data
    /// </summary>
    private void LoadSaveState()
    {
        CurrentSave = JsonUtility.FromJson<SaveState>(PlayerPrefs.GetString(saveName));
        if(CurrentSave == null)
        {
            CurrentSave = new SaveState();
        }
    }

    /// <summary>
    /// This function writes the current saved data
    /// </summary>
    public void WriteSaveState()
    {
        PlayerPrefs.SetString(saveName,JsonUtility.ToJson(CurrentSave));
        PlayerPrefs.Save();
    }

    /// <summary>
    /// This function reloads actual scene
    /// </summary>
    public void Reload()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }
}

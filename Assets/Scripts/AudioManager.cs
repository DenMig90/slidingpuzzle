﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource musicSource;
    public AudioSource soundSource;

    private void Awake()
    {
        GameManager.instance.audioManager = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        musicSource.loop = true;
        soundSource.loop = false;
        musicSource.mute = GameManager.instance.CurrentSave.musicMuted;
        soundSource.mute = GameManager.instance.CurrentSave.soundMuted;
    }

    /// <summary>
    /// This function plays a one shot audioclip
    /// </summary>
    /// <param name="clip">the clip to be played</param>
    public void PlaySound(AudioClip clip)
    {
        soundSource.PlayOneShot(clip);
    }

    /// <summary>
    /// This function sets the music to play and starts it
    /// </summary>
    /// <param name="clip">the music to be played</param>
    public void PlayMusic(AudioClip clip)
    {
        musicSource.clip = clip;
        musicSource.Play();
    }

    /// <summary>
    /// This function stops the music currently playing if any
    /// </summary>
    public void StopMusic()
    {
        musicSource.Stop();
    }

    /// <summary>
    /// This function sets the muted state of the music
    /// </summary>
    /// <param name="mute">muted state</param>
    public void MuteMusic(bool mute)
    {
        musicSource.mute = mute;
        if(GameManager.instance.CurrentSave != null)
        {
            GameManager.instance.CurrentSave.musicMuted = mute;
        }
    }

    /// <summary>
    /// This function sets the muted state of the sound
    /// </summary>
    /// <param name="mute">muted state</param>
    public void MuteSound(bool mute)
    {
        soundSource.mute = mute;
        if (GameManager.instance.CurrentSave != null)
        {
            GameManager.instance.CurrentSave.soundMuted = mute;
        }
    }
}
